import datetime
import logging
import psycopg2
from config import ConfigReader
from psycopg2 import sql
from os import system

class App(ConfigReader):

    def __init__(self, configfile, section):
        logging.basicConfig(
            filename='error.log', 
            level=logging.ERROR,
            format='%(asctime)s %(levelname)-8s %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S')
        super().__init__(configfile='database.ini', section='postgresql')
        try:
            params = super().config()
            self.conn = psycopg2.connect(**params)
            self.cur = self.conn.cursor()
        except psycopg2.OperationalError as e:
            logging.error(e)
            raise e

    def __repr__(self):
        dsn_parameters = self.conn.get_dsn_parameters()
        return f"Connected to database '{dsn_parameters['dbname']}' on host '{dsn_parameters['host']}' as user '{dsn_parameters['user']}'"    

    def __del__(self):
        if hasattr(self, "cur"):
            self.cur.close()
            self.conn.close()

    def create_tables(self):
        create_shapefamily_table_sql = sql.SQL('''
                CREATE TABLE "Shapefamily" (
                Id                            SERIAL PRIMARY KEY,
                Designation                   VARCHAR(255) UNIQUE, 
                Description                   VARCHAR(255),
                Corners                       INT,
                Dimensions                    INT
            )''')
        
        create_shape_table_sql = sql.SQL('''
                CREATE TABLE "Shape" (
                Id                            SERIAL PRIMARY KEY,
                ShapefamilyId                 INT, 
                Designation                   VARCHAR(255),
                Description                   VARCHAR(255),
                SideLengths                   INT[],
                Angles                        INT[],
                FOREIGN KEY(ShapefamilyId) REFERENCES "Shapefamily"(id) ON DELETE CASCADE
            )''')
        
        create_colour_table_sql = sql.SQL('''
                CREATE TABLE "Colour" (
                Id                            SERIAL PRIMARY KEY,
                Designation                   VARCHAR(255) UNIQUE,
                Value                         VARCHAR(7)
            )''')
        
        create_shapecolourmanagement_table_sql = sql.SQL('''
                CREATE TABLE "ShapeColourManagement" (
                Id                            SERIAL PRIMARY KEY,
                ShapeId                       INT,
                ColourId                      INT,
                FOREIGN KEY(ShapeId) REFERENCES "Shape"(id) ON DELETE CASCADE,
                FOREIGN KEY(ColourId) REFERENCES "Colour"(id) ON DELETE CASCADE
            )''')
        
        try:
            self.cur.execute(create_shapefamily_table_sql)
            self.cur.execute(create_shape_table_sql)
            self.cur.execute(create_colour_table_sql)
            self.cur.execute(create_shapecolourmanagement_table_sql)
            self.conn.commit()
            print(f'Tables created successfully!')
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print(f'Tables could not be created:\n{e}')

    def create_shape_family(self, designation, description, corners, dimensions):
        create_shape_family_sql = sql.SQL('INSERT INTO "Shapefamily" (designation, description, corners, dimensions) VALUES (\'' + designation + '\', \'' + description + '\', \'' + corners + '\', \'' + dimensions + '\')')
        try:
            self.cur.execute(create_shape_family_sql)
            self.conn.commit()
            print('Shape Family created successfully!')

        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shape Family couldn\'t be created:')
            print(e)

    def get_shape_families(self):
        get_shape_family_sql = sql.SQL('SELECT * FROM "Shapefamily" ORDER BY id ASC')
        try:
            self.cur.execute(get_shape_family_sql)
            shape_families = self.cur.fetchall()
            return shape_families
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shape Families couldn\'t be gotten:')
            print(e)

    def get_shape_family(self, shape_family_id):
        get_shape_family_sql = sql.SQL('SELECT * FROM "Shapefamily" WHERE Id = ' + shape_family_id + ' ORDER BY id ASC')
        try:
            self.cur.execute(get_shape_family_sql)
            shape_family = self.cur.fetchall()
            return shape_family
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shape Family couldn\'t be gotten:')
            print(e)

    def create_colour(self, designation, value):
        create_colour_sql = sql.SQL('INSERT INTO "Colour" (designation, value) VALUES (\'' + designation + '\', \'' + value + '\')')
        try:
            self.cur.execute(create_colour_sql)
            self.conn.commit()
            print('Colour created successfully!')
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Colour couldn\'t be created:')
            print(e)

    def get_colours(self):
        get_colour_sql = sql.SQL('SELECT * FROM "Colour" ORDER BY id ASC')
        try:
            self.cur.execute(get_colour_sql)
            colours = self.cur.fetchall()
            return colours
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Colours couldn\'t be gotten:')
            print(e)

    def create_shape(self, shapefamilyid, designation, description, sidelengths, angles):
        create_shape_sql = sql.SQL('INSERT INTO "Shape" (shapefamilyid, designation, description, sidelengths, angles) VALUES (\'' + shapefamilyid + '\', \'' + designation + '\', \'' + description + '\', ARRAY ' + sidelengths + ', ARRAY ' + angles + ')')
        try:
            self.cur.execute(create_shape_sql)
            self.conn.commit()
            print('Shape created successfully!')
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shape couldn\'t be created:')
            print(e)

    def get_newest_shape_id(self):
        get_newest_shape_sql = sql.SQL('SELECT id FROM "Shape" ORDER BY id DESC LIMIT 1')
        try:
            self.cur.execute(get_newest_shape_sql)
            newest_shape_id = self.cur.fetchall()
            return newest_shape_id
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shape couldn\'t be gotten:')
            print(e)

    def create_shape_colour_relation(self, shape_id, colour_id):
        create_shape_colour_relation_sql = sql.SQL('INSERT INTO "ShapeColourManagement" (shapeid, colourid) VALUES (\'' + shape_id + '\', \'' + colour_id + '\')')
        try:
            self.cur.execute(create_shape_colour_relation_sql)
            self.conn.commit()
            print('Shape Colour Relation created successfully!')
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shape Colour Relation couldn\'t be created:')
            print(e)

    def get_shapes(self):
        get_shape_sql = sql.SQL('SELECT * FROM "Shape" ORDER BY id ASC')
        try:
            self.cur.execute(get_shape_sql)
            shapes = self.cur.fetchall()
            return shapes
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shapes couldn\'t be gotten:')
            print(e)

    def update_shape(self, shape_id, designation, description, sidelengths, angles):
        update_shape_sql = sql.SQL('UPDATE "Shape" SET designation = \'' + designation + '\', description = \'' + description + '\', sidelengths = ARRAY ' + sidelengths + ', angles = ARRAY ' + angles + ' WHERE id = ' + shape_id)
        try:
            self.cur.execute(update_shape_sql)
            self.conn.commit()
            print('Shape edited successfully!')
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shape couldn\'t be edited:')
            print(e)

    def delete_shape_family(self, shape_family_id):
        delete_shape_family_sql = sql.SQL('DELETE FROM "Shapefamily" WHERE id = ' + shape_family_id)
        try:
            self.cur.execute(delete_shape_family_sql)
            self.conn.commit()
            print('Shape Family deleted successfully')
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shape Family couldn\'t be deleted:')
            print(e)

    def delete_colour(self, colour_id):
        delete_colour_sql = sql.SQL('DELETE FROM "Colour" WHERE id = ' + colour_id)
        try:
            self.cur.execute(delete_colour_sql)
            self.conn.commit()
            print('Colour deleted successfully')
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Colour couldn\'t be deleted:')
            print(e)

    def delete_shape(self, shape_id):
        delete_shape_sql = sql.SQL('DELETE FROM "Shape" WHERE id = ' + shape_id)
        try:
            self.cur.execute(delete_shape_sql)
            self.conn.commit()
            print('Shape deleted successfully')
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shape couldn\'t be deleted:')
            print(e)

    def search_shapes(self, search_term):
        search_shape_sql = sql.SQL('SELECT * FROM "Shape" WHERE designation LIKE \'%' + search_term + '%\' OR description LIKE \'%' + search_term + '%\' ORDER BY id ASC')
        try:
            self.cur.execute(search_shape_sql)
            shapes = self.cur.fetchall()
            return shapes
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shapes couldn\'t be gotten:')
            print(e)

    def filter_shapes(self, colour_id):
        filter_shape_sql = sql.SQL('SELECT * FROM "Shape" INNER JOIN "ShapeColourManagement" ON "Shape".id = "ShapeColourManagement".shapeid WHERE "ShapeColourManagement".colourid = ' + colour_id + ' ORDER BY "Shape".id ASC')
        try:
            self.cur.execute(filter_shape_sql)
            shapes = self.cur.fetchall()
            return shapes
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
            print('Shapes couldn\'t be gotten:')
            print(e)