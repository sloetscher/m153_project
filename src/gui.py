import psycopg2
import re
from os import system
from backend import App

class AppGui():
    @classmethod
    def run(self):
        try:
            DataAccessObject = App('database.ini', 'postgresql')
        except psycopg2.OperationalError as e:
            print(f'Unable to connect to database: {e}')
            print('Enter any key to exit')
            input()
            system('cls')
            exit()

        userInput = ''
        while userInput != '0':
            system('cls')
            print('What do you want to do:')
            print('1       create tables')
            print('2       create shape family')
            print('3       create colour')
            print('4       create shape')
            print('5       edit shape')
            print('6       delete shape family')
            print('7       delete colour')
            print('8       delete shape')
            print('9       view shape')
            print('0       quit')

            userInput = input()
            match userInput:
                case '0':
                    print('Exited.')

                case '1':
                    DataAccessObject.create_tables()

                case '2':
                    valid = True
                    print('Designation: ')
                    designation = input()
                    print('Description: ')
                    description = input()
                    print('Corners: ')
                    corners = input()
                    if not corners.isnumeric():
                        valid = False
                    print('Dimensions: ')
                    dimensions = input()
                    if not dimensions.isnumeric():
                        valid = False
                    if valid:
                        DataAccessObject.create_shape_family(designation, description, corners, dimensions)
                    else:
                        print('Validation Error!')

                case '3':
                    valid = True
                    pattern = re.compile("#[0-9A-F]{6}")
                    print('Designation: ')
                    designation = input()
                    print('Value (in the format #000000): ')
                    value = input()
                    if pattern.match(value):
                        DataAccessObject.create_colour(designation, value)
                    else:
                        print('Validation Error!')

                case '4':
                    valid = True
                    print('What colour would you like your shape to have?')
                    colours = DataAccessObject.get_colours()
                    for item in colours:
                        print(f'{item[0]}: {item[1]}')
                    colour_id = input()
                    if not colour_id.isnumeric():
                        valid = False
                    print('What Parentshape would you like your shape to belong to?')
                    shape_families = DataAccessObject.get_shape_families()
                    for item in shape_families:
                        print(f'{item[0]}: {item[1]}')
                    shape_family_id = input()
                    if not shape_family_id.isnumeric():
                        valid = False
                    print('Designation:')
                    designation = input()
                    print('Description:')
                    description = input()
                    selected_shape_family = DataAccessObject.get_shape_family(shape_family_id)
                    index = 1
                    sidelengths = []
                    print(selected_shape_family[0][3])
                    while index <= int(selected_shape_family[0][3]):
                        print(f'Side length {index}: ')
                        sidelengths.append(input())
                        if not sidelengths[index - 1].isnumeric():
                            valid = False
                        else:
                            sidelengths[index - 1] = int(sidelengths[index - 1])
                        index += 1
                    jndex = 1 # Puns
                    angles = []
                    while jndex <= int(selected_shape_family[0][3]):
                        print(f'Angle {jndex}: ')
                        angles.append(input())
                        if not angles[jndex - 1].isnumeric():
                            valid = False
                        else:
                            angles[jndex - 1] = int(angles[jndex - 1])
                        jndex += 1
                    if valid:
                        DataAccessObject.create_shape(shape_family_id, designation, description, str(sidelengths), str(angles))
                        newest_shape_id = DataAccessObject.get_newest_shape_id()
                        DataAccessObject.create_shape_colour_relation(str(newest_shape_id[0][0]), colour_id)
                    else:
                        print('Validation Error')

                case '5':
                    system('cls')
                    valid = True
                    print('Which shape would you like to edit?')
                    shapes = DataAccessObject.get_shapes()
                    for item in shapes:
                        print(f'{item[0]}: {item[2]}')
                    shape_id = input()
                    if not shape_id.isnumeric():
                        print('Invalid Id')
                        break
                    selected_shape = {}
                    for item in shapes:
                        if item[0] == int(shape_id):
                            selected_shape = item
                    
                    print('Designation: ' + selected_shape[2])
                    print('New Designation: ')
                    designation = input()
                    print('Description: ' + selected_shape[3])
                    print('New Description:')
                    description = input()
                    print('Sidelengths:')
                    for side in selected_shape[4]:
                        print('     ' + str(side))
                    sidelengths = []
                    index = 1
                    for side in selected_shape[4]:
                        print('New side number ' + str(index) + ':')
                        sidelengths.append(input())
                        if not sidelengths[index - 1].isnumeric():
                            valid = False
                        else:
                            sidelengths[index - 1] = int(sidelengths[index - 1])
                        index += 1
                    print('Angles:')
                    for angle in selected_shape[5]:
                        print('     ' + str(angle))
                    angles = []
                    jndex = 0
                    for angle in selected_shape[5]:
                        print('New angle number ' + str(jndex) + ':')
                        angles.append(input())
                        if not str(angles[jndex - 1]).isnumeric():
                            valid = False
                        else:
                            angles[jndex - 1] = int(angles[jndex - 1])
                        jndex += 1

                    if valid:
                        DataAccessObject.update_shape(shape_id, designation, description, str(sidelengths), str(angles))
                    else:
                        print('Validation Error')
                
                case '6':
                    valid = True
                    print('Which shape family would you like to delete?')
                    shape_families = DataAccessObject.get_shape_families()
                    for item in shape_families:
                        print(f'{item[0]}: {item[1]}')
                    shape_family_id = input()
                    if not shape_family_id.isnumeric():
                        valid = False
                    if valid:
                        DataAccessObject.delete_shape_family(shape_family_id)
                    else:
                        print('Validation Error')
                
                case '7':
                    valid = True
                    print('Which colour would you like to delete?')
                    colours = DataAccessObject.get_colours()
                    for item in colours:
                        print(f'{item[0]}: {item[1]}')
                    colour_id = input()
                    if not colour_id.isnumeric():
                        valid = False
                    if valid:
                        DataAccessObject.delete_colour(colour_id)
                    else:
                        print('Validation Error')
                
                case '8':
                    valid = True
                    print('Which shape would you like to delete?')
                    shapes = DataAccessObject.get_shapes()
                    for item in shapes:
                        print(f'{item[0]}: {item[2]}')
                    shape_id = input()
                    if not shape_id.isnumeric():
                        valid = False
                    if valid:
                        DataAccessObject.delete_shape(shape_id)
                    else:
                        print('Validation Error')

                case '9':
                    print('Would you like to:')
                    print('s:       Search for a shape')
                    print('f:       Filter for shapes by colour')
                    print('v:       View all shapes')
                    choice = input()
                    if choice == 's' or choice == 'f' or choice == 'v':
                        if choice == 's':
                            print('Please enter your search term:')
                            search_term = input()
                            shapes = DataAccessObject.search_shapes(search_term)
                            for item in shapes:
                                print(f'{item[0]}: {item[2]}, {item[3]}')
                        if choice == 'f':
                            print('Which colour would you like to filter by?')
                            colours = DataAccessObject.get_colours()
                            for item in colours:
                                print(f'{item[0]}: {item[1]}')
                            colour_id = input()
                            if not colour_id.isnumeric():
                                print('Invalid Input')
                                break
                            shapes = DataAccessObject.filter_shapes(colour_id)
                            for item in shapes:
                                print(f'{item[0]}: {item[2]}, {item[3]}')
                        if choice == 'v':
                            shapes = DataAccessObject.get_shapes()
                            for item in shapes:
                                print(f'{item[0]}: {item[2]}, {item[3]}')

                    else:
                        print('Invalid Input')

                case _:
                    print('Invalid command')
                    input()
            input()
            system('cls')